package com.api.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * https://www.javainuse.com/devOps/docker/docker-networking
 * https://github.com/tanerdiler/spring-boot-microservice-eureka-zuul-docker
 * 
 * @author satkumar12
 *
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableZuulProxy
public class ApiGateWay {

	public static void main(String[] args) {
		ConfigurableApplicationContext applicationContext = SpringApplication.run(ApiGateWay.class, args);
		String[] beans = applicationContext.getBeanDefinitionNames();
		for (String string : beans) {
			System.err.println(string);
		}

	}

}
