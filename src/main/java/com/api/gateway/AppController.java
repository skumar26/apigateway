package com.api.gateway;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AppController {

	@GetMapping("/info_1")
	public String status() {
		return "Working";
	}

	@GetMapping("/time_1")
	public String statustime() {
		return "Working";
	}
}
