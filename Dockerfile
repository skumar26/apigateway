# Start with a base image containing Java runtime
FROM openjdk:8-jdk-alpine

# Add Maintainer Info
LABEL maintainer="efficient.satish@gmail.com"

# Add a volume pointing to /tmp
VOLUME /tmp

# Make port 8990 available to the world outside this container
EXPOSE 8990

# The application's jar file
ARG JAR_FILE=target/ApiGateway.jar

# Add the application's jar to the container
ADD ${JAR_FILE} ApiGateway.jar

# Run the jar file 
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/ApiGateway.jar"]
